#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int fd;
int clients[100];

void accept_client(void)
{
	struct sockaddr_in clientaddr;
	socklen_t clen = sizeof clientaddr;

	int cfd = accept(fd, (struct sockaddr*)&clientaddr, &clen);

	if(cfd == -1)
	{
		perror("accept");
		close(fd);
		exit(-1);
	}

	char addrstr[INET_ADDRSTRLEN];
	printf("%d: Got client %s:%d\n",cfd, inet_ntop(clientaddr.sin_family, &clientaddr.sin_addr.s_addr, addrstr, INET_ADDRSTRLEN), ntohs(clientaddr.sin_port));

	int i;
	for(i = 0; i < 100; ++i)
	{
		if(clients[i] == -1)
		{
			clients[i] = cfd;
			return;
		}
	}

	close(cfd);
}

int main(void)
{
	int i;
	for(i = 0; i < 100; ++i)
		clients[i] = -1;

	fd = socket(PF_INET, SOCK_STREAM, 0);

	if(fd == -1)
	{
		perror("socket");
		exit(-1);
	}

	struct sockaddr_in bindaddr;
	memset(&bindaddr, 0, sizeof bindaddr);

	bindaddr.sin_family = AF_INET;
	bindaddr.sin_port = htons(1234);
	bindaddr.sin_addr.s_addr = INADDR_ANY;

	if(bind(fd, (struct sockaddr*)&bindaddr, sizeof bindaddr) == -1)
	{
		perror("bind");
		close(fd);
		exit(-1);
	}

	if(listen(fd, 5) == -1)
	{
		perror("listen");
		close(fd);
		exit(-1);
	}

	fd_set rfd,wfd;
	int numevents;
	int highest;
	
	while(1)
	{
		FD_ZERO(&rfd);
		FD_ZERO(&wfd);

		FD_SET(fd, &rfd);
		highest = fd;

		for(i = 0; i < 100; ++i)
		{
			if(clients[i] != -1) 
			{
				FD_SET(clients[i], &rfd);
				if(clients[i] > highest)
					highest = clients[i];
			}
		}
		
		numevents = select(highest+1, &rfd, &wfd, NULL, NULL);

		if(numevents <= 0)
		{
			perror("select");
			close(fd);
			exit(-1);
		}

		if(FD_ISSET(fd, &rfd))
		{
			accept_client();
		}

		for(i = 0; i < 100; ++i)
		{
			if(clients[i] != -1 && FD_ISSET(clients[i], &rfd))
			{
				char buf[100];
				ssize_t recvlen = recv(clients[i], buf, sizeof(buf)-1, 0);

				if(recvlen < 0)
				{
					perror("recv");
					close(clients[i]);
					clients[i] = -1;
				} else if(recvlen == 0) {
					printf("Lost connection from %d\n", clients[i]);
					close(clients[i]);
					clients[i] = -1;
				} else {
					printf("Received %zd bytes from %d\n", recvlen, clients[i]);
					buf[2] = 0;
					printf("%s\n", buf);
				}
			}
		}
	}
}
