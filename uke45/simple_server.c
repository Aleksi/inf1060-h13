#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(void)
{

	int fd = socket(PF_INET, SOCK_STREAM, 0);

	if(fd == -1)
	{
		perror("socket");
		exit(-1);
	}

	struct sockaddr_in bindaddr;
	memset(&bindaddr, 0, sizeof bindaddr);

	bindaddr.sin_family = AF_INET;
	bindaddr.sin_port = htons(1234);
	bindaddr.sin_addr.s_addr = INADDR_ANY;

	if(bind(fd, (struct sockaddr*)&bindaddr, sizeof bindaddr) == -1)
	{
		perror("bind");
		close(fd);
		exit(-1);
	}

	if(listen(fd, 5) == -1)
	{
		perror("listen");
		close(fd);
		exit(-1);
	}

	while(1)
	{
		struct sockaddr_in clientaddr;
		socklen_t clen = sizeof clientaddr;

		int cfd = accept(fd, (struct sockaddr*)&clientaddr, &clen);

		if(cfd == -1)
		{
			perror("accept");
			close(fd);
			exit(-1);
		}

		char addrstr[INET_ADDRSTRLEN];
		printf("Got client %s:%d\n", inet_ntop(clientaddr.sin_family, &clientaddr.sin_addr.s_addr, addrstr, INET_ADDRSTRLEN), ntohs(clientaddr.sin_port));

		send(cfd, "Hello, World!\n", 14, 0);

		//close(cfd);
	}
}
