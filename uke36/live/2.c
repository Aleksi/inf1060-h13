#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char* tulloc(size_t size)
{
	char* ptr = malloc(size*4+1);
	if(!ptr)
	{
		perror("malloc");
		return NULL;
	}

	const char* tull = "tull";

	int i;
	for(i = 0; i < (size*4); ++i)
	{
		ptr[i] = tull[i%strlen(tull)];
	}

	ptr[size*4] = 0;
	return ptr;
}
