#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <stdio.h>

struct person
{
	char* name;
	int age;
} typedef person;

person personer[100];
//fwrite(personer, antall_personer, sizeof(person), FILE);
//fread(personer, filesize/sizeof(person), sizeof(person), FILE);

struct person* person_create(void)
{
	struct person* retv = malloc(sizeof(struct person));

	if(!retv)
		return NULL;

	memset(retv, 0, sizeof(struct person));
	
	return retv;
}

void person_destroy(struct person* p)
{
	free(p->name);
	free(p);
}

char* person_get_name(struct person* p)
{
	if(!p)
		return NULL;

	return p->name;
}

int person_get_age(struct person* p)
{
	if(!p)
		return -1;

	return p->age;
}

int person_set_name(struct person* p, char* name)
{
	if(!p)
		return -1;

	if(strlen(name) > 30)
		return -3;

	free(p->name);
	p->name = strdup(name);

	if(!p->name)
		return -2;

	return 0;
}

int person_set_age(struct person* p, int age)
{
	if(!p)
		return -1;
	
	if(age > 200 || age < 0)
		return -2;

	p->age = age;

	return 0;
}



char* person_info(struct person* p)
{
	static char info[100];

	sprintf(info, "Navn: %s Alder: %d", p->name, p->age);

	return info;
}

struct person* person_read(char* str)
{
	person* p = person_create();

	char name[100];
	int age;

	int retv = sscanf(str, "Navn: %s Alder: %d", name, &age);
	printf("sscanf = %d\n", retv);

	person_set_name(p, name);
	person_set_age(p, age);

	return p;
}


int main(void)
{
	person* p1 = person_create();
	person* p2 = person_create();

	person_set_name(p1, "Test");
	person_set_name(p2, "Test2");

	person_set_name(p1, "Test3");
	person_set_age(p1, 35);

	char* p1_info = person_info(p1);

	person* p3 = person_read(p1_info);

	printf("%s\n", p1_info);
	printf("p3: %s\n", person_info(p3));

	person_destroy(p1);
	person_destroy(p2);
	person_destroy(p3);

	//

}
