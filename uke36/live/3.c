#include <stdio.h>

void printsize(short* arr)
{
	printf("Size is %d\n", sizeof(arr)/sizeof(arr[0]));
}

int main(void)
{

	int i;
	short arr[] = {2, 1, -1, 10, 22, 21, 24, 62};
	int arrlen = 6;

	for(i=0; i<sizeof(arr)/sizeof(arr[0]); i++)
		printf("[%d]: %d\n", i, *(arr+i));

	printsize(arr);
}


