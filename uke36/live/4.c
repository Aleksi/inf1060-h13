#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(void)
{
	FILE* file = fopen("test.txt", "r");

	if(!file)
	{
		perror("fopen");
		return -1;
	}

	fseek(file, 0, SEEK_END);
	long size = ftell(file);
	fseek(file, 0, SEEK_SET);

	char* contents = malloc(size+1);
	if(!contents)
	{
		perror("malloc");
		fclose(file);
		return -2;
	}

	if(fread(contents, 1, size, file) != size)
	{
		fprintf(stderr, "Read too little!\n");
		fclose(file);
		return -3;
	}

	const char *ord = "hei";
	int count = 0;

	char* token = strtok(contents, " ,.\n");

	while(token != NULL)
	{
		if(strcmp(token, ord) == 0)
			count++;

		token = strtok(NULL, " ,.");
	}

	fclose(file);
	free(contents);
	printf("Antall forekomster av '%s' er %d\n", ord, count);
	return 0;
}
