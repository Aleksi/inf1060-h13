=======================================================================
Oppgaver uke 36
=======================================================================


Oppgave 1
-----------------------------------------------------------------------

Lag en funksjon::
    
    void sum(int a, int b, int *result);

som tar summen av ``a`` og ``b``, og legger resultatet inn i 
``result``.



Oppgave 2
-----------------------------------------------------------------------

Lag funksjonen::

    char * tulloc(size_t size);

som allokerer ``size * 4 + 1`` bytes med minne og fyller området med 
så mange tilfeller av ordet "tull" som det er plass til. Minneområdets 
siste byte skal inneholde ``'\0'``. Funksjonen skal returnere en peker 
til minneområdet.



Oppgave 3
-----------------------------------------------------------------------

Gjør om denne for-løkken slik at den bruker pekere istedenfor *vanlige 
array-indekser* (slik som arr[i]) til å gå igjennom arrayen ``arr``::

    int i;
    short arr[] = {2, 1, -1, 10, 22, 21};
    int arrlen = 6;
    for(i=0; i<arrlen; i++)
        printf("[%d]: %d\n", i, arr[i]);



Oppgave 4
-----------------------------------------------------------------------

Lag et program som leser innholdet i en fil og teller antall 
forekomster av et ord.


Oppgave 5
-----------------------------------------------------------------------

Oppgaven går ut på å programmere objektorientert med C. Vi tenker oss 
at vi har en ``Person`` klasse med metodene ``setName``, ``setAge``, 
``getName`` og ``getAge``. Ettersom vi ikke har klasser i C må dette 
gjøres på en litt annen måte. Oppgaven er å finne ut av resten selv.  
Her er noen hint:

    - Lag en ``struct`` som kan inneholde informasjon om en person.
    - Lag funksjonene (``getName``, ``setAge`` osv..) slik at første 
      parameter er en peker til person structen.


A
~~~~~
Implementer enkleste mulige løsning.

B
~~~~~
Tenkt igjennom navnene du har gitt funksjonene og structen. Husk at 
alt er globalt i C. Endre navnene hvis du syns det er hensiktsmessig.

C
~~~~~
Legg til 2 funksjoner som sjekker om navnet og alderen til en person 
er satt. De skal returnere 1 (for true) eller 0 (for false).

D
~~~~
Lag en funksjon som returnerer en string med informasjon om en person.

E
~~~~
Lag en funksjon som lager en person struct fra en string med samme 
formatering som den du lagde i oppgave *D*.

F
~~~~~
Tenk igjennom hvordan feil kan håndteres på en måte som kan brukes av 
alle funksjonene du har definert så langt.

G
~~~~~
Legg inn restriksjonene:
    
    - alder ikke kan overstige 200 år.
    - navn ikke kan være mer enn 30 tegn.

Implementer feilhåndtering.



Oppgave 6
-----------------------------------------------------------------------

Lag et program som bruker datastrukturen du lagde i `Oppgave 5`_ til å 
håndtere et register med informasjon om mange personer. Programmet 
skal kunne:

    - Legge til og fjerne personer fra registeret.
    - Lagre hele registeret til fil.
    - Lese hele registeret fra fil.

Hvordan du velger å løse problemet er helt opp til deg selv.
