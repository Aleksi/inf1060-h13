#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct my_msgbuf
{
	long mtype;
	char msg[100];
};

#define ITEM1 1
#define ITEM2 2
#define ITEM3 4

/*void do_something_smart(int flags)
{
	if(flags&ITEM1)
		//Do item 1
	if(flags&ITEM2)
		//Do item 2
	if(flags&ITEM3)
		//Do item 3
}

void do_something(int do_item1, int do_item2, int do_item3)
{
	if(do_item1)
	if(do_item2)
}*/

int main(void)
{
	int mbox = msgget(IPC_PRIVATE, 0600);

	if(mbox == -1)
	{
		perror("msgget");
		return -1;
	}

	printf("Created message box: %d\n", mbox);

	struct my_msgbuf msg;
	msg.mtype = 1;
	strcpy(msg.msg, "Hello, World!");

	struct my_msgbuf *rcvmsg = malloc(sizeof(struct my_msgbuf));

	if(msgsnd(mbox, &msg, sizeof(msg), 0) == -1)
	{
		perror("msgsnd");
		return -1;
	}

	ssize_t retv = msgrcv(mbox, rcvmsg, sizeof(struct my_msgbuf), 1, 0);

	if(retv == -1)
	{
		perror("msgrcv");
		return -2;
	}

	printf("Got message: %s\n", rcvmsg->msg);

	if(msgctl(mbox, IPC_RMID, NULL) == -1)
	{
		perror("msgctl");
		return -3;
	}

	return 0;
}
