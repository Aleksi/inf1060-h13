#include <stdio.h>
#include <signal.h>
#include <sched.h>

void ctrl_c_handler(int signalnum)
{
	signal(SIGINT, ctrl_c_handler);

	fprintf(stderr, "Got control-c, number=%d\n", signalnum);
}

int main(void)
{
	if(signal(SIGINT, ctrl_c_handler) == SIG_ERR)
	{
		perror("signal");
		return -1;
	}

	while(1)
	{
		sched_yield();
	}
}
