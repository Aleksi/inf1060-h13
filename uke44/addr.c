#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>

#include <memory.h>
#include <stdlib.h>
#include <assert.h>

int main(int argc, char* argv[])
{
	assert(argc == 2);

	struct addrinfo hints, *res;
	int retv;

	memset(&hints, 0, sizeof(hints));

	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	if((retv = getaddrinfo(argv[1], "http", &hints, &res)) != 0)
	{
		fprintf(stderr, "Something went wrong with getaddrinfo: %s\n",
			gai_strerror(retv));
		return -1;
	}


	struct addrinfo *cur = res;
	char ipaddr[INET6_ADDRSTRLEN];

	while(cur)
	{
		fprintf(stderr, "Host ip: %s\n",inet_ntop(cur->ai_family, &((struct sockaddr_in*)cur->ai_addr)->sin_addr, ipaddr, INET6_ADDRSTRLEN));

		cur = cur->ai_next;
	}

	freeaddrinfo(res);

	return 0;
}
