#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int main(int argc, char* argv[])
{
	(void)argc;
	(void)argv;

	const char* msg = "Hello, World!\n";

	int fd = open("output.txt", O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);

	if(fd == -1)
	{
		perror("open");
		exit(-1);
	}

	size_t written = 0;

	while(written < strlen(msg))
	{
		ssize_t retv = write(fd, msg, strlen(msg));

		fprintf(stderr, "Wrote %zu bytes to file\n", retv);

		if(retv <= 0)
		{
			perror("write");
			exit(-2);
		}

		written += retv;
	}

	close(fd);
	return 0;
}
