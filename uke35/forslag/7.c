#include <stdio.h>
#include <string.h>

int strcmpx(unsigned char s1[], unsigned char s2[]){
	if (strlen (s2) > strlen (s1)) return 1;
	if (strlen (s2) < strlen (s1)) return -1;


	int i;
	for (i = 0; i < strlen(s1); i++) {
		if (s2[i] - s1[i] != 0) return s2[i] - s1[i];
	}
	return 0;
}

void test(unsigned char s1[], unsigned char s2[]){
	printf("strcmpx(\"%s\", \"%s\") gir %d strcmp gir %d\n",
			s1, s2, strcmpx(s1,s2), strcmp(s1, s2));
}

int main(){
	test("Abc", "Ab");
	test("Abc", "Abc");
	test("Abc", "Abcd");
	return 0;
}
