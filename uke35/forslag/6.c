int ishex(unsigned char c)
{
	if ((c >= '0' && c <= '9') 
		|| (c >= 'a' && c <= 'f')
		|| (c >= 'A' && c <= 'F'))
		return 1;
	else
		return 0;
}

int hexval(unsigned char c)
{
	if (!ishex(c))
		return -1;

	if (c >= '0' && c <= '9') return (c - '0');
	if (c >= 'a' && c <= 'f') return (c - 'a' + 10);
	if (c >= 'A' && c <= 'F') return (c - 'A' + 10);
}
