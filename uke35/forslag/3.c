#include <stdio.h>

int main(){
    short a, b, sum;
    a = 20000;  b = 20000;  sum = a+b;

    if ((a > 0 && b > 0 && sum < 0)
      || (a < 0 && b < 0 && sum > 0)) {
	printf("Overflow!\n");
    } else {
        printf("%d + %d = %d\n", a, b, sum);
    }
}
