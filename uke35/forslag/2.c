#include <stdio.h>
#include <string.h>

char strgetc (char s[], int pos) 
{
	if (pos >= strlen(s)) return 0;

	return s[pos];
}

int main(int argc, char **argv) 
{
	char *s = "Hello";
	int i;	

	for (i = 0; i < 7; i++) {
		printf("Tegnet: %c\n", strgetc (s, i));
	}
}
