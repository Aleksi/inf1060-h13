#include <stdio.h>
#include <string.h>

#define STREQ(s1,s2) (0 == strcmp((s1),(s2)))

int main()
{
	char *a = "Hello";
	char *b = "Hello2";
	char *c = "Hello2";

	if (STREQ(a,b))
		printf("Like!\n");
	else
		printf("Ulike.\n");

	if(STREQ(b,c))
		printf("Like!\n");
	else
		printf("Ulike.\n");
}
