#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isnumber(char *s) {
	int i;
	for (i = 0; i < strlen(s); i++) {
		if (s[i] < '0' || s[i] > '9') return 0;
	}
	return 1;
}

int main (int argc, char **argv)
{
	if (argc == 2) {
		char *msg = argv[1];
		printf("Tall?: %d\n", isnumber(msg));
	} else {
		printf("Feil!\n");
	}
	return 1;
}
