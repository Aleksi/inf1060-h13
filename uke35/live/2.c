#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char strgetc(char s[], int pos)
{
	if(s == NULL)
		return -1;

	int lengde = strlen(s);

	if(pos > lengde)
		return -1;

	return s[pos];
}

int main(int argc, char** argv)
{
	printf("Bokstaven på plass %d er: %c\n", atoi(argv[2]), strgetc(argv[1], atoi(argv[2])));

	return 0;
}
