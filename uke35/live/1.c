#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

int is_digit(char* str)
{
	size_t i;
	for(i = 0; i < strlen(str); ++i)
	{
		if(!isdigit(str[i]))
			return 0;
	}

	return 1;
}

int main(int argc, char* argv[])
{
	char* msg;

	if(argc < 2)
	{
		printf("USAGE: %s tekst\n", argv[0]);
		return -1;
	}

	msg = argv[1];

	printf("Input: %s\n", msg);

	if(is_digit(msg))
	{
		int tall = atoi(msg);
		printf("Tallet er: %d\n", tall);
	}

	return 0;

}
