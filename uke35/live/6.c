

int ishex(unsigned char c)
{
	if((c >= '0' && c <= '9'))
		return 1;
	
	if(c >= 'a' && c <= 'f')
		return 1;
	
	if(c >= 'A' && c <= 'F')
		return 1;

	return 0;
}

int hexval(unsigned char c)
{
	if(!ishex(c))
		return -1;

	if(c >= '0' && c <= '9')
		return c-'0';

	if(c >= 'a' && c <= 'f')
		return c-'a'+10;
}
