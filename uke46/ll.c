#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
	char* name;
	struct node* next;
} node;


void insert_list(const char* name, node** root)
{
	node* n = malloc(sizeof(node));
	n->name = strdup(name);
	n->next = *root;
	*root = n;
}

void print_list(node* root)
{
	while(root)
	{
		printf("Node: %s\n", root->name);
		root = root->next;
	}
}


int main(void)
{
	int a[10];
	node* root;

	printf("Hello, World!\n");
	
	insert_list("A", &root);
	insert_list("B", &root);
	insert_list("C", &root);

	print_list(root);

	return 0;
}
